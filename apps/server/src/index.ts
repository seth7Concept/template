import { createExpressMiddleware } from "@trpc/server/adapters/express";
import cors from "cors";
import express from "express";
import path from "path";
import { prisma } from "./prisma";
import { trpcRouter } from "./routers/index";

const PORT = 3000;

const app = express();
const appRouter = express.Router();

app.use(cors());
app.use(express.json());
app.use("/api", appRouter);

app.use("/trpc", createExpressMiddleware({ router: trpcRouter }));

app.use(express.static(path.join(__dirname, "../..", "client", "dist")));

appRouter.get("/", (_req, res) => {
  res.send("Hello from Rest API!");
});

appRouter.get("/users", async (_req, res) => {
  const users = await prisma.user.findMany();
  res.json(users);
});

app.listen(PORT || 3000, () => {
  console.log(`Server listening on port ${PORT || 3000}`);
});

export type TrpcRouter = typeof trpcRouter;

import { z } from "zod";
import { prisma } from "../prisma";
import { trpc } from "../trpc";

export const userRouter = trpc.router({
  getAllUsers: trpc.procedure.query(async () => {
    const users = await prisma.user.findMany();
    return users;
  }),
  getUserById: trpc.procedure
    .input(z.object({ ID: z.string().cuid() }))
    .query(async ({ input }) => {
      const user = await prisma.user.findFirst({
        where: {
          id: input.ID,
        },
      });
      return user;
    }),
  createUser: trpc.procedure
    .input(z.object({ name: z.string(), email: z.string().email() }))
    .mutation(async ({ input }) => {
      const user = await prisma.user.create({
        data: {
          name: input.name,
          email: input.email,
        },
      });
      return user;
    }),
});

import { trpc } from "./utils/trpc";

// type User = {
//   id: string;
//   name: string;
//   email: string;
// };

export default function App() {
  // With Vanilla Axios

  // const [hello, setHello] = useState<string>("");

  // useEffect(() => {
  //   axios.get("http://localhost:3000/api").then((res) => setHello(res.data));
  // }, []);

  // With React Query

  // const getUsers = async () => {
  //   const res = await axios.get("http://localhost:3000/api/users");
  //   return res.data;
  // };

  // const usersQuery = useQuery<User[]>(["users"], getUsers);

  // With TRPC

  const utils = trpc.useContext();

  const createUserMutation = trpc.user.createUser.useMutation({
    onSuccess() {
      utils.user.getAllUsers.refetch();
    },
  });

  const helloQuery = trpc.hello.useQuery();
  const usersQuery = trpc.user.getAllUsers.useQuery();
  const userQuery = trpc.user.getUserById.useQuery({
    ID: "clhfuk9zi00003bvgox8em21x",
  });

  if (helloQuery.isFetching) return <div>Loading...</div>;

  function handleCreateUser() {
    createUserMutation.mutate({
      name: "Samah",
      email: "s2@a.com",
    });
  }

  return (
    <>
      <div>{helloQuery.data}</div>
      {usersQuery.data?.map((user) => (
        <p key={user.name}>{user.id}</p>
      ))}
      <div>{userQuery.data?.name}</div>
      <button onClick={handleCreateUser}>Add User</button>
    </>
  );
}

import { createTRPCReact } from "@trpc/react-query";
import type { TrpcRouter } from "../../../server/src/index";

export const trpc = createTRPCReact<TrpcRouter>();
